<?php

use PHPUnit\Framework\TestCase;
use CaiqueCastro\Algorithms\WordWrap;

class WordWrapTest extends TestCase
{
	public function testItNotWrapsShortInput()
	{
		$return = WordWrap::handle('Word', 10);

		$this->assertEquals('Word', $return);
	}

	public function testWrapsSentencesWithShortWords()
	{
		$return = WordWrap::handle('These are some short words.', 10);

		$this->assertEquals("These are\nsome short\nwords.", $return);
	}

	public function testWrapsSentencesWithALongWord()
	{
		$return = WordWrap::handle('These is a sentence with a VeryLengthyWord.', 10);

		$this->assertEquals("These is a\nsentence\nwith a\nVeryLength\nyWord.", $return);
	}
}

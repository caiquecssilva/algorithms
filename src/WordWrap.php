<?php

namespace CaiqueCastro\Algorithms;

class Wordwrap
{
	public static function handle(string $input, int $limit) {
		$words = explode(" ", $input);
		$wordsCount = count($words);

		$response = "";
		$lineLength = 0;
		$wordOnLine = 0;

		for ($i = 0; $i < $wordsCount; $i++) {
			$word = $words[$i];

			if ($lineLength + mb_strlen($word) >= $limit) {
				$response .= "\n";
				$lineLength = 0;
			}

			if ($lineLength != 0) {
				$response .= " ";
				$lineLength++;
			}

			if (mb_strlen($word) > $limit) {
				$word = substr($word, 0, $limit);
				$words[$i] = substr($words[$i], $limit);
				$i--;
			}

			$response .= $word;
			$lineLength += mb_strlen($word);
		}

		// die(var_dump($words));

		return $response;
	}
}
